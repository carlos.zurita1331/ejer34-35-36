const clientes = [
  {
    id: 1,
    nombre: 'Leonor'
  },
  {
    id: 2,
    nombre: 'Jacinto'
  },
  {
    id: 3,
    nombre: 'Waldo'
  }
];
const pagos = [
  {
    id: 1,
    pago: 1000,
    moneda:'Bs'
   
  },
  {
    id: 2,
    pago: 1800,
    moneda:'Bs'
    
  }
];



const id = 1;

const getCliente=(id)=>{
  return new Promise((resolve,reject) =>{
    const cliente = clientes.find(e => e.id === id);

    if(cliente){
      resolve (cliente)
    }else{  
      reject(`No existe el cliente con el id ${id}`);
    }

  });
};

const getPago=(id)=>{
return new Promise((resolve,reject)=>{
  const pago = pagos.find(e => e.id === id);

  if(pago){
    resolve (pago)
  }else{
    reject(`No existe el pago con el id ${id}`);
  }
});
};

//a) callback
const getClientById=(id,callback) => {

  const cliente = clientes.find(e => id === id);
  const pago = pagos.find(e=>id === id)
  console.log(clientes);
  console.log(pagos);

  if(cliente && pago){
    callback(cliente,pago);
  } else {
    console.log('callback: este id no existe');
  }
  };

  getClientById(1, (clientes, pagos) => {
     console.log(`el cliente con id ${clientes.id} con el nombre: ${clientes.nombre} tiene un saldo a pagar de ${pagos.pago} en ${pagos.moneda}`)
    });
    





//b) callback hell

//c) promesa simple

// getCliente(id)
//  .then(cliente => console.log(cliente))
//  .catch(error => {
//   //Codigo a realizar cuando se rechaza la promesa
//   console.log(error);
//  });
// getPago(id)
//  .then(pago => console.log(pago))
//  .catch(error => {
//   //Codigo a realizar cuando se rechaza la promesa
//   console.log(error);
//  });

//   getCliente(id)
//  .then(cliente => {
  
//   getPago(id)
//    .then(pago => {
//     console.log('El cliente:', cliente, 'tiene un saldo a pagar de: ', pago);
//    })
//    .catch(error => {
//     //Codigo a realizar cuando se rechaza la promesa
//     console.log(error);
//    });;
//  })
//   .catch(error => {
//   //Codigo a realizar cuando se rechaza la promesa
//   console.log(error);
//   });

//   //d) promesa en cadena

// let nombre;
// getCliente(id)
//  .then(cliente => {
//    console.log(cliente);
//    nombre = cliente.nombre;
//    //Siempre colocar un return para poder encadenar un then
//  return getPago(id);
//  })
//  .then(pago => {
//    console.log('El cliente:', nombre, 'tiene un saldo a pagar de:', pago['pago'],'en', pago['moneda']);
//  })
//  .catch(error => {
//    //Codigo a realizar cuando se rechaza la promesa
//  console.log(error);
//  });


// //e) sync-await

// const getInfoUsuario = async (id) => {
//    try {
//    const cliente = await getCliente(id);
//    const pago = await getPago(id);
//    return `El cliente de id = ${cliente.id} de: ${cliente.nombre} tiene un saldo a pagar de: ${pago['pago']} en ${pago['moneda']}`;
//  }
//  catch(ex){
//    throw ex;
//  }
//   };
//   getInfoUsuario(id)
//    .then(msg => console.log(msg))
//    .catch(err => console.log(err));
  

