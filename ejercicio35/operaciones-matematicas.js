const fs = require('fs');

//operacion de suma
const crearArchivoAdd=(base=1) =>{
  try {
    let salida=''
    console.log('=====================');
    console.log(` sumas del ${base}   `);
    console.log('=====================');
    
    for (let i = 1; i <=100 ; i++) {
      salida += `${i} + ${base} = ${i + base}\n`;
    }
    console.log(salida);
    fs.writeFileSync(`./salida-operaciones/tabla-suma ${base}.txt`,salida)
    console.log(`tabla-Suma-${base}.txt`);
  } catch (error) {
    throw(error)
  }
};

  //operacion de resta
const crearArchivoRest=(base=1) =>{
  try {
    let salida=''
    console.log('=====================');
    console.log(` restas del ${base}   `);
    console.log('=====================');
    
    for (let i = 1; i <=100 ; i++) {
      salida += `${i} - ${base} = ${i - base}\n`;
    }
    console.log(salida);
    fs.writeFileSync(`./salida-operaciones/tabla-Resta ${base}.txt`,salida)
    console.log(`tabla-Resta-${base}.txt`);
  } catch (error) {
    throw(error)
  }
};


//operacion de multiplicacion
const crearArchivo = (base = 1) =>{
  try {
    let salida = ''
    console.log('=====================');
    console.log(` tabla del ${base}   `);
    console.log('=====================');
  
    for(let i = 1; i <= 100; i++){
      salida +=`${base} x ${i} = ${base * i}\n`;
    }
  
    console.log(salida);
    fs.writeFileSync(`./salida-operaciones/tabla-Multiplicar ${base}.txt`,salida);
     console.log(`tabla-Multiplicar-${base}.txt`);

      
    } catch (error) {
      throw(error)
  }

};


//operacion de division
const crearArchivoDiv = (base = 1) =>{
  try {
    let salida = ''
    console.log('=====================');
    console.log(` tabla de division ${base}   `);
    console.log('=====================');
  
    for(let i = 1; i <= 100; i++){
      salida +=`${i} ÷ ${base} = ${ i / base}\n`;
    }
  
    console.log(salida);
    fs.writeFileSync(`./salida-operaciones/tabla-dividir ${base}.txt`,salida);
     console.log(`tabla-dividir-${base}.txt`);

      
    } catch (error) {
      throw(error)
  }

};
//exportamos el archivo
module.exports={
  generarArchivo2:crearArchivoAdd,
  generarArchivo3:crearArchivoRest,
  generarArchivo:crearArchivo,
  generarArchivo4:crearArchivoDiv,
  
};